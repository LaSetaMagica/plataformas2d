using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]

public class PlayerController : MonoBehaviour
{
    public float xMovement;
    public float yMovement;
    public Animator personajeAnimator;
    public Rigidbody2D rb2d;
    SpriteRenderer myRenderer;
    //public bool facingRight = true;
    //public bool facingLeft;
    //public bool playRun = true;
    //public Animation playRun;
    //public bool isJumping;Z
    private Camera mainCamera;
    //private Vector3 moveInput;

    private LayerMask GroundLayer;
    private SpriteRenderer spriteRenderer = null;

    public int vidas = 3;
    public static bool final;


    // Start is called before the first frame update
    void Start()
    {
        personajeAnimator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        myRenderer = GetComponent<SpriteRenderer>();
        mainCamera = Camera.main;
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer == null)
        {
            Debug.Log("Sprite del jugador nulo");
        }

        final = false;
    }

    void Flip(float horizontalInput)
    {
        if (horizontalInput > 0)
        {
            if (spriteRenderer.flipX != false)
            {
                spriteRenderer.flipX = false;
            }
            else if (horizontalInput < 0)
            {
                if (spriteRenderer.flipX != true)
                {
                    spriteRenderer.flipX = true;
                }
            }
        }
    }
    //Intento de rotaci�n del personaje
    //void Flip()
    //{
    //facingRight = !facingRight;
    //myRenderer.flipX = !myRenderer.flipX;
    //}

    //Intento de rotaci�n
    // Update is called once per frame

    //private void FixedUpdate()
    //{
        //rb2d.MovePosition(transform.position + transform.right * xMovement * Time.deltaTime);
    //}

    private bool CheckGrounded()
    {
        if (Physics2D.Raycast(transform.position, Vector2.down, 0.75f, GroundLayer))
        {
            Debug.DrawRay(transform.position, Vector2.down, Color.green);
        }
        return false;
    }
    void Update()
    {
        //Rotacion personaje (no funciona)

        //Vector3 cameraFoward = mainCamera.transform.forward;
        //cameraFoward.y = 0;

        //Quaternion cameraRelativeRotation = Quaternion.FromToRotation(Vector3.forward, cameraFoward);
        //Vector3 lookToward = cameraRelativeRotation * moveInput;

        //if (moveInput.sqrMagnitude > 0)
        //{
        //Ray2D lookRay2D = new Ray2D(transform.position, lookToward);
        //transform.LookAt(lookRay2D.GetPoint(1));
        //}

        //CalculateMovement();

        if (Input.GetKey(KeyCode.W))
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.left * xMovement;
            personajeAnimator.Play("Run");
           // if (move > 0 && !facingRight)
            //{
                //Flip();
            //} 
            //else if (move < 0 && facingRight)
            //{
                //Flip();
            //}
            //personajeAnimator.SetTrigger("playRun");
            //personajeAnimator.SetBool("playRun", true);
            //personajeAnimator.SetFloat("xMovement", Mathf.Abs(move));
            //personajeAnimator.SetBool("Run", true);
        }

        if (Input.GetKey(KeyCode.W))
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.left * xMovement;
            personajeAnimator.Play("Run");
            
        }

        if (Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.right * xMovement;
            personajeAnimator.Play("Run");

        }

        if (Input.GetKeyDown("space"))
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.up * yMovement;
            personajeAnimator.Play("Jump");
        }

        if (!Input.anyKey)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            personajeAnimator.Play("Idle");
        }
    }

    //void CalculateMovement()
    //{
    //float horizontalInput = Input.GetAxisRaw("Horizontal");
    //Vector2 velocity = new Vector2(horizontalInput * yMovement, rb2d.velocity.y);

    //if (Input.GetKeyDown(KeyCode.Space) && CheckGrounded())
    //{
    //velocity.y = yMovement;
    //}

    //if (rb2d != null)
    //{
    //rb2d.velocity = velocity;
    //}

    //Flip(horizontalInput);
    //}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            vidas--;
            if (vidas == 0)
            {
                final = true;
                Time.timeScale = 0;
                Destroy(gameObject);
            }
        }
    }
}
