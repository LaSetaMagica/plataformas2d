using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text scoreText;
    public static int score;
    public int monedas = 3;
    public GameObject Coin;
    public static bool gameOver;

    // Start is called before the first frame update
    void Start()
    {
        gameOver = false;
        score = 0;
        scoreText.text = "Coin: " + monedas;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = score.ToString();
    }

    private void FixedUpdate()
    {
        if (monedas == 0)
        {
            monedas--;
            scoreText.text = "Coin: " + monedas;
        }
    }

    public void PlayerDied()
    {
        gameOver = true;
    }

}
